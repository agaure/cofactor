function [ E ] = test2(M)
    Mt = M .* log(M+(M==0));
    E = -sum(Mt, 2);
end