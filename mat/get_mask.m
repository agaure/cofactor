function [ mask ] = get_mask( hide, Y_tr )
    mask = zeros(size(Y_tr));
    ones = find(Y_tr);
    mask(ones(randperm(numel(ones), round(numel(ones)*(100-hide)/100)))) = 1;
end

