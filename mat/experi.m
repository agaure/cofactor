r = 5;
maxiters = 50;
w_co_mat = [];
wo_co_mat = [];
w_co_mod_mat = [];
leml_mat = []
hn = 1;
tr = 1;
st = 20;
ed = 40;
for h = st:20:ed
    pW = [];
    pWO = [];
    pWC = [];
    for j = 1:tr
        pWO(j,:) = without_co(h, r, maxiters);
        pW(j,:) = with_co(h, r, maxiters);
        pWC(j,:) = with_co_mod(h, r, maxiters);
        %h = h + 10;
    end
    pW;
    pWO;
    w_co_mat(hn, :) = sum(pW, 1) ./ tr;
    wo_co_mat(hn,:) = sum(pWO, 1) ./ tr;
    w_co_mod_mat(hn,:) = sum(pWC, 1) ./ tr;
    file = strcat('multi_res/hide/weird/fres',num2str(h),'.mat');
    save(file, 'pW', 'pWO', 'pWC');
    figure
    plot([pW(:,1), pWO(:,1), pWC(:, 1)], 'LineWidth', 2);
    ylabel(strcat('Precision@',num2str(1)));
    xlabel('Trials');
    legend('with co','without co', 'with co mod');
    saveas(gcf, strcat('multi_res/hide/weird/hide', num2str(h), '.jpg'));
    hn = hn + 1;
end
file = strcat('multi_res/hide/weird/final.mat');
save(file, 'w_co_mat', 'wo_co_mat', 'w_co_mod_mat');
figure
x = st:20:ed;
plot(x, [w_co_mat(:,1), wo_co_mat(:,1), w_co_mod_mat(:,1)], 'LineWidth', 2);
ylabel(strcat('Precision@',num2str(1)));
xlabel('Percent of hidden data');
legend('with co','without co', 'with co mod');
saveas(gcf, strcat('multi_res/hide/weird/final_g', num2str(h), '.jpg'));