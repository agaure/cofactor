close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);

[N L] = size(Y_tr);

Ls = 80;
Lu = L - Ls;

D = size(X_tr,2);
K = round(0.8*L);

M = Y_tr(:, :)'*Y_tr;
%--------------------------
Mpp = M(1:Ls, Ls+1:end)';
% Mpn = Y_tr(:,Ls+1:end)'*(~Y_tr(:,1:Ls));
% Mnp = ~Y_tr(:,Ls+1:end)'*Y_tr(:,1:Ls);
% Mnn = ~Y_tr(:,Ls+1:end)'*(~Y_tr(:,1:Ls) * eye(Ls));
%--------------------------

lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
lambda5 = lambda;
r = 5;

theta = randn(N,K);
beta = randn(L,K);
gamma = randn(L,K);
W = randn(K,D);


maxiters = 50;

%Y_tr = Y_tr(:,1:Ls);
    
tic;
for iter=1:maxiters
    parfor n=1:N
        omega = (0.5./(theta(n,:)*beta(1:Ls,:)')).*tanh((theta(n,:)*beta(1:Ls,:)')/2);
        theta_tmp = inv(bsxfun(@times,beta(1:Ls,:)',omega)*beta(1:Ls,:) + lambda1*eye(K))*(sum(bsxfun(@times,beta(1:Ls,:)',Y_tr(n,1:Ls)-0.5),2) + lambda1*W*X_tr(n,:)');
        theta(n,:) = theta_tmp';
    end
    parfor l=1:Ls
        omega = (0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);
        
%        omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
%        omega2 = omega2';
        
        A = (bsxfun(@times,theta',omega')*theta) %+ bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
        b = sum(bsxfun(@times,theta',Y_tr(:,l)'-0.5),2) %+ sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2);
      
        beta_tmp = A\b;
        beta(l,:) = beta_tmp';
    end
%     parfor l=Ls+1:L
%         omega = 0 %(0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);
%         
%         omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
%         omega2 = omega2';
%         
%         A = bsxfun(@times,gamma',omega2')*gamma + 2*lambda2*eye(K);
%         b = sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2) + gamma(l,:)'; %%%%%
%       
%         beta_tmp = A\b;
%         beta(l,:) = beta_tmp';
%     end
    
    parfor l=1:L  
        omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
        A = bsxfun(@times,beta',omega2)*beta + 2*lambda3*eye(K);
        b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
        if(l > Ls)
            b = b + beta(l,:)';
        end
        gamma_tmp = A\b;
        gamma(l,:) = gamma_tmp';
    end
    
    W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
    W = W';
    
    %--------------------
    %Sp = normalizeMatrix(Mpp);
    Sp = Mpp./repmat(sum(Mpp,2),1,Ls);
%     Spp = normCooccur(Mpp);
%     Spn = normCooccur(Mpn);
%     Snp = normCooccur(Mnp);
%     Snn = normCooccur(Mnn);
%     Sfin = Spp - Spn - Snp; + Snn;
    %Sfin = normalizeMatrix(Sfin);
    %--------------------
    
    Psi = inv(gamma(1:Ls,:)'*gamma(1:Ls,:) + lambda5*eye(K))*gamma(1:Ls,:)'*beta(1:Ls,:);
    beta_u = gamma(Ls+1:end,:)*Psi;
    
    theta_test = X_te*W';
    
    Y_test = sigmoid(theta_test*[beta(1:Ls,:)' beta_u']);
    
    [p] = evalPrecision(Y_test, Y_te, 5);
    fprintf('Seen+Unseen Labels (ours ): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));  
    
    %----------------------
    
%     K_us = kernelPoly(gamma(Ls+1:end,:),gamma(1:Ls,:),2);
%     K_ss = kernelPoly(gamma(1:Ls,:),gamma(1:Ls,:),2);
%     K_us = kernelRBF(gamma(Ls+1:end,:),gamma(1:Ls,:),10);
%     K_ss = kernelRBF(gamma(1:Ls,:),gamma(1:Ls,:),10);    
%     beta_u = K_us*inv(K_ss + lambda5*eye(Ls))*beta;
%     
%     theta_test = X_te*W';
%     
%     Y_test = sigmoid(theta_test*[beta' beta_u']);
%     
%     [p] = evalPrecision(Y_test, Y_te, 5);
%     fprintf('Seen+Unseen Labels (ours-kernel ): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));      
    
    %----------------------
    
%     beta_u = M(:,Ls+1:end)'*beta; % A baseline: beta of each unseen label is a combination of the beta's of seen labels
%     Y_test = sigmoid(theta_test*[beta' beta_u']);
%     
%     [p] = evalPrecision(Y_test, Y_te, 5); 
%     fprintf('Seen+Unseen Labels (unnorm): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));        
    
    %-------------------
    
    beta_u = Sp*gamma(1:Ls,:); % A baseline: beta of each unseen label is a combination of the beta's of seen labels
    Y_test = sigmoid(theta_test*[beta(1:Ls,:)' beta_u']);
    
    [p] = evalPrecision(Y_test, Y_te, 5); 
    fprintf('Seen+Unseen Labels (norm ): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));    
    
%     beta_u = Sfin*beta; % A baseline: beta of each unseen label is a combination of the beta's of seen labels
%     Y_test = sigmoid(theta_test*[beta' beta_u']);
%     
%     [p] = evalPrecision(Y_test, Y_te, 5); 
%     fprintf('Seen+Unseen Labels (costa): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n\n',iter,p(1),p(3),p(5));    
    %-------------------
end
toc;
