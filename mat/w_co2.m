close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);

[N L] = size(Y_tr);

D = size(X_tr,2);
K = round(0.2*L);

M = Y_tr'*Y_tr;
load('masked_20.mat');
% hide = 20;
% 
% mask = zeros(size(Y_tr));
% mask(randperm(numel(Y_tr), round(numel(Y_tr)*(100-hide)/100))) = 1;
Y_tr = Y_tr .* mask;

lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
lambda5 = lambda;
theta = randn(N,K);
beta = randn(L,K);
gamma = randn(L,K);
W = randn(K,D);
r = 10;
res = [];
ans = zeros(1,5);
maxiters = 100;
c1 = 0.3;%L/(N+L);
c2 = 0.7;%N/(N+L);
prev_p = -inf;
tic;
for iter=1:maxiters
    for n=1:N
        omega = (0.5./(theta(n,:)*beta')).*tanh((theta(n,:)*beta')/2);
        theta_tmp = inv(bsxfun(@times,(beta .* repmat(mask(n,:)', 1, K))',omega)*(beta .* repmat(mask(n,:)', 1, K)) + lambda1*eye(K))*(sum(bsxfun(@times,(beta .* repmat(mask(n,:)', 1, K))',Y_tr(n,:)-0.5),2) + lambda1*W*X_tr(n,:)');
        theta(n,:) = theta_tmp';
    end
    for l=1:L
        omega = (0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);
        omega = omega * c1;
        
        omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
        omega2 = omega2' * c2;
        
        A = (bsxfun(@times,(theta .* repmat(mask(:,l), 1, K))',omega')*(theta .* repmat(mask(:,l), 1, K)) + bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
        b = (sum(bsxfun(@times,(theta .* repmat(mask(:,l), 1, K))',c1*(Y_tr(:,l)'-0.5)),2)) + sum(bsxfun(@times,gamma',c2*(0.5*M(l,:)-0.5*r)),2);
      
        beta_tmp = A\b;
        beta(l,:) = beta_tmp';
    end
    
    for l=1:L     
        omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
        A = bsxfun(@times,beta',omega2)*beta + lambda5*eye(K);
        b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
        gamma_tmp = A\b;
        gamma(l,:) = gamma_tmp';
    end
    
    W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
    W = W';
    
    % Approach 1 (direct prediction)
    theta_test = X_te*W';
    Y_test = sigmoid(theta_test*beta');
    Y_train = sigmoid(theta*beta');
    
    [p] = evalPrecision(Y_test, Y_te, 5); 
    [p2] = evalPrecision(Y_train, Y_tr, 5);
    %fprintf('Tr Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p2(1),p2(3),p2(5));
    res(iter,:) = p;
    if ans(1) < p(1)
       ans = p;
    end
    if mod(iter, 1) == 0
        fprintf('Te Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f, so-far = %f\n',iter,p(1),p(3),p(5), ans(1));
    end;
    if (p(1) - prev_p) < 0.0001
       % break;
    end
    prev_p = p(1);
end

file1 = strcat('multi_res/res_w_co_r', num2str(r),'.mat');
save(file1, 'res');
figure;
plot(res , 'LineWidth', 2);
toc;