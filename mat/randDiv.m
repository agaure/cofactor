function [C]=randDiv(M) 
[n,m]=size(M);
[c,idx]=sort(rand(n,1));
C=M(idx,:);
end 