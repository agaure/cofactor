close all;rng('default');
load('multi_res/res_w_co_r5.mat');
fr = res(:, 1);
load('multi_res/res_wo_co_r5.mat');
fr(:, 2) = res(:, 1);

for i=1:1
    figure%('Visible', 'off');
    plot(fr, 'LineWidth', 2);
    ylabel(strcat('Precision@',num2str((i-1)*2+1)));
    xlabel('Iterations');
    legend('with co','without co');
    %saveas(gcf, strcat('results/pre_', num2str((i-1)*2+1), '.jpg'));
end