function [ mask ] = get_mask2( hide, Y_tr )
    mask = zeros(size(Y_tr));
    mask(randperm(numel(Y_tr), round(numel(Y_tr)*(100-hide)/100))) = 1;
end

