close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);

[N L] = size(Y_tr);

pcc = 50;
Ls = round(L*pcc/100);
Lu = L - Ls;
D = size(X_tr,2);
K = round(0.8*L);
M = Y_tr(:,1:Ls)'*Y_tr;
 %--------------------------
Mpp = M(:, Ls+1:end)';
 %--------------------------
lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
lambda5 = lambda;
r = 10;
theta = randn(N,K);
beta = randn(Ls,K);
gamma = randn(L,K);
W = randn(K,D);
maxiters = 100;
tauc = 0;
tauc1 = 0;
p = [];
ans = zeros(1,5);
ans1 = zeros(1,5);
    %Y_tr = Y_tr(:,1:Ls);

tic;
for iter=1:maxiters
    for n=1:N
        omega = (0.5./(theta(n,:)*beta')).*tanh((theta(n,:)*beta')/2);
        theta_tmp = inv(bsxfun(@times,beta',omega)*beta + lambda1*eye(K))*(sum(bsxfun(@times,beta',Y_tr(n,1:Ls)-0.5),2) + lambda1*W*X_tr(n,:)');
        theta(n,:) = theta_tmp';
    end
    for l=1:Ls
        omega = (0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);

         omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
         omega2 = omega2';

         A = (bsxfun(@times,theta',omega')*theta + bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
         b = sum(bsxfun(@times,theta',Y_tr(:,l)'-0.5),2) + sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2);

         beta_tmp = A\b;
         beta(l,:) = beta_tmp';
    end

    for l=1:L     
         omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
         A = bsxfun(@times,beta',omega2)*beta + lambda3*eye(K);
         b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
         gamma_tmp = A\b;
         gamma(l,:) = gamma_tmp';
    end
    
    W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
    W = W';

        %--Linear gamma -> beta--------------------
    Sp = Mpp./repmat(sum(Mpp,2),1,Ls); 
    Psi = inv(gamma(1:Ls,:)'*gamma(1:Ls,:) + lambda5*eye(K))*gamma(1:Ls,:)'*beta;
    beta_u = gamma(Ls+1:end,:)*Psi; 
    theta_test = X_te*W';
    Y_test = sigmoid(theta_test*[beta' beta_u']);
    [p] = evalPrecision(Y_test, Y_te, 5);
    tm = ones(size(Y_test));
    auc = compute_AUC(Y_te, Y_test, tm);
    if ans(1) < p(1)
        ans = p;
        save('wcobib.mat', 'Y_test');
    end
        
    if auc > tauc
        tauc =auc;
    end
    fprintf('L:, Seen+Unseen Labels(ML-LCS): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f %f %f \n', iter,p(1),p(3),p(5), ans(1), tauc);  
        %--------------------------------------

        %--Convex combination--------------------
    beta_u = Sp*beta; % A baseline: beta of each unseen label is a combination of the beta's of seen labels
    Y_test = sigmoid(theta_test*[beta' beta_u']);

    [p] = evalPrecision(Y_test, Y_te, 5); 
    tm = ones(size(Y_test));
    auc1 = compute_AUC(Y_te, Y_test, tm);
              
    if ans1(1) < p(1)
        ans1 = p;
        save('wcobib1.mat', 'Y_test');
    end
    if auc1 > tauc1
        tauc1 =auc1;
    end
    fprintf('L:, Seen+Unseen Labels (norm ): Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n\n', iter,p(1),p(3),p(5), ans1(1), tauc1);    
        %--------------------------------------
end
toc;