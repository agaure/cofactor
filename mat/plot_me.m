close all;rng('default');
load('results/r10_res_.mat');
res = [];
x = 20:10:80;
for i=1:3
    res(1:size(mxL),1) = mxL(:,i);
    res(1:size(mxC),2) = mxC(:,i);
    res
    figure%('Visible', 'off');
    plot(x,res, 'LineWidth', 2);
    ylabel(strcat('Precision@',num2str((i-1)*2+1)));
    xlabel('percentage of seen classes');
    legend('ML-LCS','COSTA');
    saveas(gcf, strcat('results/pre_', num2str((i-1)*2+1), '.jpg'));
end