function [ ZZ ] = normCooccur(Z)
    ci = sum(Z', 1);
    ZZ = bsxfun(@rdivide, Z', ci)';
end

