
function [pre] = with_co(hide, r, maxiters)
    load('bibtex.mat');

    [X_tr] = normalizeMatrix(X_tr);
    [X_te] = normalizeMatrix(X_te);

    [N L] = size(Y_tr);

    D = size(X_tr,2);
    K = round(0.5*L);

    M = Y_tr'*Y_tr;

    load('w_masked.mat');
%     mask = zeros(size(Y_tr));
%     mask(randperm(numel(Y_tr), round(numel(Y_tr)*(100-hide)/100))) = 1;

    Y_tr = Y_tr .* mask;
    
    lambda = 1;
    lambda1 = lambda;
    lambda2 = lambda;
    lambda3 = lambda;
    lambda4 = lambda;
    lambda5 = lambda;
    theta = randn(N,K);
    beta = randn(L,K);
    gamma = randn(L,K);
    W = randn(K,D);
    res = [];
    pre = zeros(5, 1);
    
    for iter=1:maxiters
        for n=1:N
            omega = (0.5./(theta(n,:)*beta')).*tanh((theta(n,:)*beta')/2);
            theta_tmp = inv(bsxfun(@times,beta',omega)*beta + lambda1*eye(K))*(sum(bsxfun(@times,beta',Y_tr(n,:)-0.5),2) + lambda1*W*X_tr(n,:)');
            theta(n,:) = theta_tmp';
        end
        for l=1:L
            omega = (0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);

            omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
            omega2 = omega2';

            A = ((bsxfun(@times,theta',omega')*theta) + bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
            b = sum(bsxfun(@times,theta',Y_tr(:,l)'-0.5),2) + sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2);

            beta_tmp = A\b;
            beta(l,:) = beta_tmp';
        end

        for l=1:L     
            omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
            A = bsxfun(@times,beta',omega2)*beta + lambda5*eye(K);
            b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
            gamma_tmp = A\b;
            gamma(l,:) = gamma_tmp';
        end

        W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
        W = W';

        % Approach 1 (direct prediction)
        theta_test = X_te*W';
        Y_test = sigmoid(theta_test*beta');
        Y_train = sigmoid(theta*beta');

        [p] = evalPrecision(Y_test, Y_te, 5); 
        [p2] = evalPrecision(Y_train, Y_tr, 5);
        %fprintf('Tr Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p2(1),p2(3),p2(5));
        if mod(iter, 10) == 0
            fprintf('Tr Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));
        end
        res(iter, :) = p;
        if(pre(1) < p(1))
            pre = p;
        end
    end
    fprintf('co: Hide %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',hide,pre(1),pre(3),pre(5));
    file1 = strcat('multi_res/hide/weird/res_w_co_hide', num2str(hide),'.mat');
    save(file1, 'res');