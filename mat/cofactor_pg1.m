close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);

[N L] = size(Y_tr);

D = size(X_tr,2);
K = round(0.8*L);

M = Y_tr'*Y_tr;

lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
theta = randn(N,K);
beta = randn(L,K);
gamma = randn(L,K);
W = randn(K,D);
r = 0.5;

D_tr = [X_tr, Y_tr];
C = randDiv(D_tr);
bt_size = 200;
dt = 1;

maxiters = 20;

Y_tr = C(:, 1837: 1836+159);
X_tr = C(:, 1: 1836);
T = fix(N/bt_size);

gam = 1;
A_old = 0;
b_old = 0;
W_old = 0;
Ag_old = 0;
bg_old = 0;
    
tic;
for t = 0:T
    st_ind = t*bt_size + 1;
    end_ind = st_ind+bt_size-1;
    if(end_ind > N)
        end_ind = N;
    end
    Y = Y_tr(st_ind:end_ind, :);
    X = X_tr(st_ind:end_ind, :);
    theta_bt = theta(st_ind:end_ind, :);
    gam = (t+2)^(-0.5);
    for n=1:bt_size
        kappa = Y(n,:)-0.5;
        for iter=1:20
            omega = (0.5./(theta_bt(n,:)*beta')).*tanh((theta_bt(n,:)*beta')/2);
            theta_tmp = inv(bsxfun(@times,beta',omega)*beta + lambda1*eye(K))*(sum(bsxfun(@times,beta', kappa),2) + lambda1*W*X(n,:)');
            theta_bt(n,:) = theta_tmp';
        end
    end
    theta(st_ind:end_ind,:) = theta_bt;
    
    for l=1:L
        omega = (0.5./(theta_bt*beta(l,:)')).*tanh((theta_bt*beta(l,:)')/2);
        omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
        omega2 = omega2';
        
        A = (bsxfun(@times,theta_bt',omega')*theta_bt + bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
        A = gam*A + (1-gam)*A_old;
        b = sum(bsxfun(@times,theta_bt',Y(:,l)'-0.5),2) + sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2);
        b = gam*b + (1-gam)*b_old;
        
        beta_tmp = A\b;
        beta(l,:) = beta_tmp';
        A_old = A;
        b_old = b;
    end
    
    for l=1:L     
        omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
        A = bsxfun(@times,beta',omega2)*beta;
        A = gam*A + (1-gam)*Ag_old;
        b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
        b = gam*b + (1-gam)*bg_old;
        gamma_tmp = A\b;
        gamma(l,:) = gamma_tmp';
        Ag_old = A;
        bg_old = b;
    end
    
    W = inv(X'*X + lambda4*eye(D))*X'*theta_bt;
    W = W';
    W = gam*W + (1-gam)*W_old;
    W_old = W;
    
    % Approach 1 (direct prediction)
    theta_test = X_te*W';
    Y_test = sigmoid(theta_test*beta');
    Y_train = sigmoid(theta_bt*beta');
    
    [p] = evalPrecision(Y_test, Y_te, 5); 
    [p2] = evalPrecision(Y_train, Y, 5);
    fprintf('Tr Data: batch %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',t,p2(1),p2(3),p2(5));
    fprintf('Te Data: batch %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',t,p(1),p(3),p(5));
end
toc;
