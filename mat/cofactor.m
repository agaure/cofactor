close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);


[N L] = size(Y_tr);

D = size(X_tr,2);
K = round(0.8*L);
M = Y_tr'*Y_tr;
% m_sum = sum(M,2);
% M = M./(m_sum*m_sum');
% M = M*nnz(M);
% M = log(M);
% k = 10;
% M = max(M - log(k),0);
% 
lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
theta = randn(N,K);
beta = randn(L,K);
gamma = randn(L,K);
W = randn(K,D);


maxiters = 200;
    
tic;
for iter=1:maxiters    
    theta_tmp = inv(beta'*beta + lambda1*eye(K))*(beta'*Y_tr' + lambda1*W*X_tr');
    theta = theta_tmp';
    A0 = theta'*theta;
    for l=1:L
        [tmp nz] = find(M(l,:)~=0);
        gamma_nz = gamma(nz,:);
        A = (A0 + gamma_nz'*gamma_nz + lambda2*eye(K));
        if ~isempty(nz)
            b = theta'*Y_tr(:,l) + gamma_nz'*M(nz,l);
        else
            b = theta'*Y_tr(:,l);
        end
        beta_tmp = A\b;
        beta(l,:) = beta_tmp';
    end
    
    for l=1:L
        [tmp nz] = find(M(l,:)~=0);
        beta_nz = beta(nz,:);
        A = (beta_nz'*beta_nz + lambda3*eye(K));
        if ~isempty(nz)        
            b = beta_nz'*M(nz,l);
        else
            b = zeros(K,1);
        end
        gamma_tmp = A\b;
        gamma(l,:) = gamma_tmp';
    end
    
    W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
    W = W';
    
    % Approach 1 (direct prediction)
    theta_test = X_te*W';
    Y_test = theta_test*beta';
    Y_train = theta*beta';
    
    [p] = evalPrecision(Y_test, Y_te, 5); 
    [p2] = evalPrecision(Y_train, Y_tr, 5);
    fprintf('Tr Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p2(1),p2(3),p2(5));
    fprintf('Te Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));
end
toc;
