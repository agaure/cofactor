w_co = [];
wo_co = [];
tr = 15;
hn = 1;
st = 10;
ed = 70;
for h = st:10:ed
    file = strcat('multi_res/hide/fres',num2str(h),'.mat');
    load(file);
    w_co(hn, :) = sum(pW, 1) ./ tr;
    wo_co(hn, :) = sum(pWO, 1) ./ tr;
    hn = hn + 1;
end
figure;
x = st:10:ed;
plot(x, [w_co(:,1), wo_co(:,1)], 'LineWidth', 2);
ylabel(strcat('Precision@',num2str(1)));
xlabel('Percentage of hidden data');
legend('with co','without co');
saveas(gcf, strcat('multi_res/hide/hide_final.jpg'));
