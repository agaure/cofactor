function [ S, U ] = partTrTs( n, Yt )
    S = [];
    U = [];
    c = 1;
    M = Yt' * Yt;
    M(logical(eye(size(M)))) = 0;
    while( size(S) < n)
        if(size(S)+c > n)
            c = n - size(S);
        end
        Mt = normalize(M);
        E = calcEnt(Mt);
        E(S) = -Inf;
        [sE, Inds] = sort(E, 'descend');
        S(end+1:end+c) = Inds(1:c);
        M(S,:) = 0;
        M(:,S) = 0;
    end
    U = setdiff([1:159], S);
end

function [ E ] = calcEnt(M) 
    Mt = M .* log(M+(M==0));
    E = -sum(Mt, 2);
end

function [ ZZ ] = normalize(Z)
    ci = sum(Z', 1);
    ZZ = bsxfun(@rdivide, Z', ci)';
    ZZ(isnan(ZZ)) = 0;
end