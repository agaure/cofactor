function [p] = leml()
    %load('mediamill_1.mat');
    load('bibtex.mat');
    load('masked_80.mat');
    Y_tr = Y_tr .* mask;
    Y_tr = sparse(Y_tr);
    X_tr = sparse(X_tr);
    Y_te = sparse(Y_te);
    X_te = sparse(X_te);
    [W H wall_time] = train_ml(Y_tr, X_tr, Y_te, X_te, '-k 48, -t 50');
    theta_test = W*X_te';
    theta_test = theta_test';
    beta_test = H;
    size(W)
    size(H)
    Y_test = theta_test*beta_test;
    [p] = evalPrecision(Y_test, Y_te, 5); 
    tm = ones(size(Y_test));
    auc = compute_AUC(Y_te, Y_test, tm);
    fprintf('Te Data: Iteration Prec@1 = %f, Prec@3 = %f, Prec@5 = %f, auc = %f \n',p(1),p(3),p(5), auc);
    save('data.mat', 'Y_test');
end