function [ZZ] = normalizeMatrix(Z)
    Zt = Z';
    normZ = sqrt(sum((Zt.^2), 1));
    ZZ =  bsxfun(@rdivide, Zt, normZ);
    ZZ = ZZ';
end