close all;rng('default');
load('bibtex.mat');

[X_tr] = normalizeMatrix(X_tr);
[X_te] = normalizeMatrix(X_te);

[N L] = size(Y_tr);

D = size(X_tr,2);
K = round(0.8*L);

M = Y_tr'*Y_tr;

lambda = 1;
lambda1 = lambda;
lambda2 = lambda;
lambda3 = lambda;
lambda4 = lambda;
theta = randn(N,K);
beta = randn(L,K);
gamma = randn(L,K);
W = randn(K,D);
r = 0.5;

D_tr = [X_tr, Y_tr];
C = randDiv(D_tr, 100);
size(C)
maxiters = 2;
bt_size = 100;
dt = 1;
tic;
Y_tr = C(:, 1837: 1836+159);
st_ind = 1;
for iter=1:maxiters
    for end_ind = bt_size : bt_size : N
        theta1 = theta(st_ind:end_ind, :);
        Y_tr1 = Y_tr(st_ind:end_ind, :);
        for n=1:bt_size
            omega = (0.5./(theta1(n,:)*beta')).*tanh((theta1(n,:)*beta')/2);
            theta_tmp = inv(bsxfun(@times,beta',omega)*beta + lambda1*eye(K))*(sum(bsxfun(@times,beta',Y_tr(n,:)-0.5),2) + lambda1*W*X_tr(n,:)');
            theta(n,:) = theta_tmp';
        end
    for l=1:L
        omega = (0.5./(theta*beta(l,:)')).*tanh((theta*beta(l,:)')/2);
            
        omega2 = ((0.5*(M(l,:)+r))./(beta(l,:)*gamma')).*tanh((beta(l,:)*gamma')/2);
        omega2 = omega2';
        
        A = (bsxfun(@times,theta',omega')*theta + bsxfun(@times,gamma',omega2')*gamma + lambda2*eye(K));
        b = sum(bsxfun(@times,theta',Y_tr(:,l)'-0.5),2) + sum(bsxfun(@times,gamma',0.5*M(l,:)-0.5*r),2);
      
        beta_tmp = A\b;
        beta(l,:) = beta_tmp';
     end
    
     for l=1:L     
        omega2 = ((0.5*(M(:,l)'+r))./(gamma(l,:)*beta')).*tanh((gamma(l,:)*beta')/2);
        A = bsxfun(@times,beta',omega2)*beta;
        b = sum(bsxfun(@times,beta',0.5*M(:,l)'-0.5*r),2);
        gamma_tmp = A\b;
        gamma(l,:) = gamma_tmp';
     end
    
     W = inv(X_tr'*X_tr + lambda4*eye(D))*X_tr'*theta;
     W = W';
    
     % Approach 1 (direct prediction)
     theta_test = X_te*W';
     Y_test = sigmoid(theta_test*beta');
     Y_train = sigmoid(theta*beta');
    
     [p] = evalPrecision(Y_test, Y_te, 5); 
     [p2] = evalPrecision(Y_train, Y_tr, 5);
     fprintf('Tr Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p2(1),p2(3),p2(5));
     fprintf('Te Data: Iteration %d, Prec@1 = %f, Prec@3 = %f, Prec@5 = %f\n',iter,p(1),p(3),p(5));
     end
end
toc;
